/*
 * Ship.h
 *
 *  Created on: Oct 3, 2016
 *      Author: ysi
 */

#ifndef SHIP_H_
#define SHIP_H_

#include "Bullet.h"

using namespace std;

class Ship: public Object {

public:

	Ship() {
		m_counter = 0;
	}

	virtual ~Ship() {
	}

	virtual void setTexture(sf::Texture *texture_ptr,
			sf::Texture *bullet_texture_ptr) {
		m_texture_ptr = texture_ptr;
		m_bullet_texture_ptr = bullet_texture_ptr;

		m_sprite.setTexture(*m_texture_ptr);
	}

	virtual void setDirection(sf::Vector2f direction) {
		if (m_health > 0.0f) {
			m_direction = direction;
		}
	}

	virtual void setFire() {
		if (m_health > 0.0f) {
			m_fire = true;
		}
	}

	virtual void update(long iter, std::list<Object*> &objects) {
		// End game animation
		if (m_health < 0.0f) {
			m_counter++;
			if (m_counter % 10 < 5) {
				m_sprite.setTexture(*m_texture_ptr);
			} else {
				m_sprite.setTexture(empty_texture);
			}
			return;
		}

		// Update ship position
		m_position += sf::Vector2f(m_direction.x * m_speed.x,
				m_direction.y * m_speed.y);
		m_sprite.setPosition(m_position);

		// Fire bullets
		if (m_fire && iter % 5 == 0) {
			Bullet *bullet = new Bullet(m_bullet_texture_ptr);
			bullet->setSpeed(sf::Vector2f(0, -10));
			bullet->setHealth(19);
			bullet->setDamage(10);
			bullet->setPosition(m_position + sf::Vector2f(13, -8));
			objects.push_back(bullet);

			bullet = new Bullet(m_bullet_texture_ptr);
			bullet->setSpeed(sf::Vector2f(0, -10));
			bullet->setHealth(19);
			bullet->setDamage(10);
			bullet->setPosition(m_position + sf::Vector2f(31, -8));
			objects.push_back(bullet);
		}
		m_fire = false;
	}

	virtual void takeDamage(float damage) {
		m_health -= damage;
	}

	virtual int getCounter() {
		return m_counter;
	}

protected:
	sf::Vector2f m_direction = { 0, 0 };
	bool m_fire = false;
	sf::Texture *m_bullet_texture_ptr = nullptr;
	sf::Texture empty_texture;
	int m_counter;
};

#endif /* SHIP_H_ */
