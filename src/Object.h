/*
 * Object.h
 *
 *  Created on: Oct 3, 2016
 *      Author: ysi
 */

#ifndef OBJECT_H_
#define OBJECT_H_

#include <list>
#include <SFML/Graphics.hpp>

using namespace std;

class Object {
public:
	Object(sf::Texture *texture_ptr = nullptr) : m_texture_ptr(texture_ptr) {
	}

	virtual void setTexture(sf::Texture *texture_ptr = nullptr) {
		m_texture_ptr = texture_ptr;
	}

	virtual ~Object() {
	}

	sf::FloatRect getGlobalBounds() {
		return m_sprite.getGlobalBounds();
	}

	virtual sf::Sprite& getSprite() {
		return m_sprite;
	}

	virtual sf::Vector2f& getPosition() {
		return m_position;
	}

	virtual void setPosition(sf::Vector2f position) {
		m_position = position;
		m_sprite.setPosition(position);
	}

	virtual sf::Vector2f& getSpeed() {
		return m_speed;
	}

	virtual void setSpeed(sf::Vector2f speed) {
		m_speed = speed;
	}

	virtual float getHealth() {
		return m_health;
	}

	virtual void setHealth(float health) {
		m_health = health;
	}

	virtual float giveDamage() {
		return m_damage;
	}

	virtual void takeDamage(float damage) {
		m_health -= damage;
	}

	virtual bool isDead() {
		return m_dead;
	}

	virtual void update(long iter, std::list<Object*> &objects) {
		m_position += m_speed;
	}

	void setDamage(float damage) {
		m_damage = damage;
	}

	void setStartHealth(float health) {
		m_startHealth = health;
	}

protected:
	sf::Texture *m_texture_ptr = nullptr;
	bool m_dead = false;
	float m_startHealth = 100; // ??? Why? ???
	sf::Vector2f m_position = {0, 0};
	sf::Vector2f m_speed = {0, 0};
	float m_health = 100;
	float m_damage = 0;

	sf::Sprite m_sprite;
};

#endif /* OBJECT_H_ */
