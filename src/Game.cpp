#include "Game.h"

#include <cassert>

using namespace std;

Game::Game(size_t width, size_t height) :
		m_window("Vertical shooter", sf::Vector2u(width, height)), m_width(
				width), m_height(height) {
	srand(time(NULL));

	m_ship_texture.loadFromFile("images/PlayerShip2.png");
	m_bullet_texture.loadFromFile("images/Bullet1.png");
	m_asteroid_texture.loadFromFile("images/a1.png");

	// First object is player's ship
	CreateShip();

	RestartClock();
}

void Game::DestroyScene() {
	for (auto object = m_objects.begin(); object != m_objects.end(); ++object) {
		delete *object;
	}
	m_objects.clear();
}

void Game::CreateShip() {
	m_ship = new Ship();
	m_ship->setTexture(&m_ship_texture, &m_bullet_texture);
	m_ship->setPosition(sf::Vector2f(0.5 * m_width, 0.8 * m_height));
	m_ship->setSpeed(sf::Vector2f(5, 4));
	m_ship->setHealth(100);
	m_ship->setDamage(200);
	m_objects.push_back(m_ship);
}

Game::~Game() {
	DestroyScene();
}

sf::Time Game::GetElapsed() {
	return m_elapsed;
}

void Game::RestartClock() {
	// fixed time-clock
	m_elapsed += m_clock.restart();
}

void Game::SubElapsed(float frametime) {
	m_elapsed -= sf::seconds(frametime);
}

Window* Game::GetWindow() {
	return &m_window;
}

bool Game::OutOfScene(sf::Vector2f position) {
	const float spacing = 0.5f * m_width;
	if (position.x < -spacing || position.x > m_width + spacing
			|| position.y < -spacing || position.y > m_height + spacing)
		return true;
	return false;
}

void Game::HandleInput() {
	// Input handling.
	sf::Vector2f direction = { 0, 0 };
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		--direction.x;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		++direction.x;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		--direction.y;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		++direction.y;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
		m_ship->setFire();
	}
	m_ship->setDirection(direction);
}

void Game::Update() {
	++m_iter;
	m_window.Update();
	if (m_ship->getCounter() > 180) {
		DestroyScene();
		CreateShip();
	}
	UpdateObjects();
}

void Game::UpdateObjects() {
	// Update objects
	auto object = m_objects.begin();
	while (object != m_objects.end()) {
		(*object)->update(m_iter, m_objects);
		++object;
	}

	// Create new asteroids.
	if (m_iter % 20 == 0) {
		Asteroid *asteroid = new Asteroid(&m_asteroid_texture);
		asteroid->setSpeed(sf::Vector2f(-2 + rand() % 5, 3 + rand() % 3));
		asteroid->setHealth(100);
		asteroid->setDamage(200);
		asteroid->setPosition(sf::Vector2f(rand() % m_width, -200));
		m_objects.push_back(asteroid);
	}

	// Collide every object with others
	auto object1 = m_objects.begin();
	while (object1 != m_objects.end()) {
		// No need to compare an object with itself
		auto object2 = object1;
		object2++;
		assert(object1 != object2);
		while (object2 != m_objects.end()) {
			Collide(*object1, *object2);
			++object2;
		}
		++object1;
	}

	// Delete dead or out-of-scene objects
	object = m_objects.begin();
	while (object != m_objects.end()) {
		if ((*object)->isDead() || OutOfScene((*object)->getPosition())) {
			// Call destructor
			delete *object; // *object is a pointer!
			// Remove pointer from list
			object = m_objects.erase(object);
		}
		++object;
	}
}

void Game::Collide(Object *object1, Object *object2) {
	assert(object1);
	assert(object2);
	sf::FloatRect bound1 = object1->getGlobalBounds();
	sf::FloatRect bound2 = object2->getGlobalBounds();
	sf::FloatRect result;
	if (bound1.intersects(bound2, result)) {
		// This is the only place where damage is given or taken.
		// Taking damage may affect the ability to give damage.
		float damage1 = object1->giveDamage();
		float damage2 = object2->giveDamage();
		object1->takeDamage(damage2);
		object2->takeDamage(damage1);
	}
}

void Game::Render() {
	m_window.BeginDraw(); // Clear.
	for (auto object_ptr : m_objects) {
		m_window.Draw(object_ptr->getSprite());
	}
	m_window.EndDraw(); // Display.
}
