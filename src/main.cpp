#include "Game.h"
#include <iostream>

constexpr float frametime = 1.0f / 60.0f;

using namespace std;
int main(int argc, char** argv) {

	// Create game object.
	Game game(800,600);

	// Game loop.
	while (!game.GetWindow()->IsDone()) {

		if (game.GetElapsed().asSeconds() >= frametime) {
			// Do something 60 times a second.
			game.HandleInput();
			game.Update();
			game.Render();
			game.SubElapsed(frametime); // fixed time-clock
		}

		sf::sleep(sf::seconds(1.0f/300)); // Small delay to release CPU
		game.RestartClock();
	}

	return 0;
}
