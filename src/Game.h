#pragma once

#include <list>
#include "Window.h"
#include "Ship.h"
#include "Bullet.h"
#include "Asteroid.h"

class Game {

public:
	Game(size_t width, size_t height);
	~Game();

	void HandleInput();
	void Update();
	void Render();

	Window* GetWindow();

	sf::Time GetElapsed();
	void SubElapsed(float frametime);
	void RestartClock();

	void DestroyScene();
	void CreateShip();
	void UpdateObjects();
	bool OutOfScene(sf::Vector2f position);
	void Collide(Object *object1, Object *object2);

private:
	Window m_window;
	sf::Clock m_clock;
	sf::Time m_elapsed;

	long m_iter = 0;

	size_t m_width, m_height;

	sf::Texture m_ship_texture;
	sf::Texture m_bullet_texture;
	sf::Texture m_asteroid_texture;

	Ship *m_ship = nullptr;

	std::list<Object*> m_objects;
};
