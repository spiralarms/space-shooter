/*
 * Asteroid.h
 *
 *  Created on: Oct 3, 2016
 *      Author: ysi
 */

#ifndef ASTEROID_H_
#define ASTEROID_H_

#include "Object.h"

class Asteroid: public Object {
public:
	Asteroid(sf::Texture *texture_ptr) {
		setTexture(texture_ptr);
	}

	virtual ~Asteroid() {
	}

	virtual void setTexture(sf::Texture *texture_ptr = nullptr) {
		Object::setTexture(texture_ptr);
		m_sprite.setTexture(*texture_ptr);
	}

	virtual void update(long iter, std::list<Object*> &objects) {
		m_position += m_speed;
		m_sprite.setPosition(m_position);
	}

	virtual void takeDamage(float damage) {
		m_health -= damage;
		if (m_health < 0.0f) {
			m_dead = true;
		}
	}
};

#endif /* ASTEROID_H_ */
